#include "ExecutableProgram.h"

namespace {
    using std::shared_ptr;
    using std::list;
}

ExecutableProgram::ExecutableProgram(){

}

ExecutableProgram::~ExecutableProgram(){

}

unsigned int ExecutableProgram::getSize() const{
    return instructions.size();
}

bool ExecutableProgram::hasNextStatement() const{
    return instructions.size() > 0;
}

shared_ptr<Statement> ExecutableProgram::getNextStatement(){
    auto result = instructions.front();
    instructions.pop_front();
    return result;
}

void ExecutableProgram::addInstruction(shared_ptr<Statement> instr){
    instructions.push_back(instr);
}
