#pragma once
#include "Statement.h"

class Store : public Statement{
    public:
    Store(memory_type raddr, register_type rnum) : addr{raddr}, num{rnum}
    {
    }

    ~Store(){

    }

    void execute(OS& os) const{
        os.setMemory(addr, os.getRegisters(num));
    }

    private:
    memory_type addr;
    register_type num;
};
