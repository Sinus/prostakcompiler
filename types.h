#pragma once

#include <memory>
#include <vector>

typedef int time_type;
typedef int register_type;
typedef int memory_type;
typedef int32_t memory_cell_type;
typedef int32_t register_content_type;

class ExecutableProgram;

typedef std::shared_ptr<std::vector<ExecutableProgram>> Execs;
