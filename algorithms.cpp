#include "algorithms.h"

std::shared_ptr<SchedulingAlgorithm> createFCFSScheduling(){
    FCFS sa;
    return std::make_shared<FCFS>(sa);
}

std::shared_ptr<SchedulingAlgorithm> createRRScheduling(time_type quantum){
    RRS sa(quantum);
    return std::make_shared<RRS>(sa);
}
std::shared_ptr<SchedulingAlgorithm> createSJFScheduling(){
    SJF sa;
    return std::make_shared<SJF>(sa);
}
