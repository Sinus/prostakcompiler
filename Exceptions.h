#pragma once

#include <exception>

#define DEFINE_EXCEPTION(exc) class exc : public std::exception {}

DEFINE_EXCEPTION(NoCPUException);
DEFINE_EXCEPTION(NoRAMException);
DEFINE_EXCEPTION(IllegalArgumentException);
DEFINE_EXCEPTION(UnknownInstructionException);
DEFINE_EXCEPTION(InvalidAddressException);
DEFINE_EXCEPTION(InvalidRegisterException);
DEFINE_EXCEPTION(DivisionByZeroException);