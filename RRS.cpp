#include "Statement.h"

#include "RRS.h"

#include <set>

RRS::~RRS()
{
}

void RRS::executePrograms(Execs execs, OS& os) {
	while (!execs->empty())
	{
		std::set<int> deleteMe;
		for (unsigned int i = 0; i < execs->size(); i++)
		{
			for (int j = 0; j < quantum; j++)
			{
				if (execs->at(i).hasNextStatement())
					execs->at(i).getNextStatement()->execute(os);
				else
				{
					deleteMe.insert(i);
					break;
				}
			}
		}
		//because we're deleting from back towards the beginning, 
        //nothing should break inside the vector
		for (std::set<int>::iterator it = deleteMe.end(); it != 
                deleteMe.begin(); it--)
		{
			execs->erase(execs->begin()+*it);
		}
	}
}
