#pragma once

#include "SchedulingAlgorithm.h"

class FCFS
	: public SchedulingAlgorithm
{
public:
	FCFS();
	~FCFS();
	void executePrograms(Execs execs, OS& os);
};
