#pragma once
#include <vector>
#include <memory>
#include "types.h"
#include "OS.h"
#include "Exceptions.h"

class Statement
{
public:
	virtual ~Statement(){

    }
	virtual void execute(OS&) const = 0;
};
