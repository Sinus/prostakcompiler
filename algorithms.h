#pragma once

#include <memory>
#include "types.h"
#include "SchedulingAlgorithm.h"
#include "FCFS.h"
#include "RRS.h"
#include "SJF.h"

std::shared_ptr<SchedulingAlgorithm> createFCFSScheduling();
std::shared_ptr<SchedulingAlgorithm> createRRScheduling(time_type quantum);
std::shared_ptr<SchedulingAlgorithm> createSJFScheduling();
