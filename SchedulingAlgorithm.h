#pragma once

#include <memory>
#include <vector>

#include "ExecutableProgram.h"

class OS;

class SchedulingAlgorithm
{
public:
	SchedulingAlgorithm(){}
	virtual ~SchedulingAlgorithm(){}
	virtual void executePrograms(Execs, OS&) = 0;
};

