#pragma once
#include "Statement.h"

class Load : public Statement{
    public:
    Load(register_type rnum, memory_type raddr) : num{rnum}, addr{raddr}
    {
    }

    ~Load(){

    }

    void execute(OS& os) const{
        os.setRegisters(num, os.getMemory(addr));
    }

    private:
    register_type num;
    memory_type addr;
};
