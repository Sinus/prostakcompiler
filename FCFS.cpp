#include "FCFS.h"
#include "ExecutableProgram.h"
#include "Statement.h"

FCFS::FCFS()
{
}


FCFS::~FCFS()
{
}

void FCFS::executePrograms(Execs execs, OS& os){
	for (ExecutableProgram& program : *execs.get())
	{
		while (program.hasNextStatement())
		{
			program.getNextStatement()->execute(os);
		}
	}
}
