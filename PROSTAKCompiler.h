#pragma once

#include <list>

#include "SchedulingAlgorithm.h"
#include "types.h"

Execs compilePrograms(const std::list<std::string>& programs);
