#include "PROSTAKCompiler.h"
#include <string>
#include <regex>
#include <boost/regex.hpp>
#include <iostream>
#include <sstream>
#include <memory>

#include "ExecutableProgram.h"
#include "Statement.h"
#include "Set.h"
#include "Load.h"
#include "Store.h"
#include "Add.h"
#include "Sub.h"
#include "Mul.h"
#include "Div.h"
#include "Println.h"

namespace{
    using std::list;
    using std::string;
    using std::istringstream;
    using std::getline;
    using std::shared_ptr;
    using std::make_shared;
    using std::stoi;
    using std::vector;
   
    //insert instruction to Executableprogram
    template<typename A1, typename A2, typename T>
    void insertStatement(A1 arg1, A2 arg2, ExecutableProgram& exe)
    {
        T instr(arg1, arg2);
        auto p = make_shared<T>(instr);
        exe.addInstruction(p);
    }
}

Execs compilePrograms(const list<string>& _programs)
{
	vector<ExecutableProgram> execs_;
    list<string> programs = _programs;

    //paterns to look for instructions
    boost::regex setPattern("^\\s*SET\\s+R0*([0-9]+)\\s+0*([0-9]+)\\s*$");
    boost::regex loadPattern("^\\s*LOAD\\s+R0*([0-9]+)\\s+M0*([0-9]+)\\s*$");
    boost::regex storePattern("^\\s*STORE\\s+M0*([0-9]+)\\s+R0*([0-9]+)\\s*$");
    boost::regex addPattern("^\\s*ADD\\s+R0*([0-9]+)\\s+R0*([0-9]+)\\s*$");
    boost::regex subPattern("^\\s*SUB\\s+R0*([0-9]+)\\s+R0*([0-9]+)\\s*$");
    boost::regex mulPattern("^\\s*MUL\\s+R0*([0-9]+)\\s+R0*([0-9]+)\\s*$");
    boost::regex divPattern("^\\s*DIV\\s+R0*([0-9]+)\\s+R0*([0-9]+)\\s*$");
    boost::regex printlnPattern("^\\s*PRINTLN\\s+R0*([0-9]+)\\s*$");
    boost::smatch match;


    while (!programs.empty())
    {
        auto whole = programs.front();
        programs.pop_front();

        istringstream iss(whole);
        string curr = "";
        ExecutableProgram exe;
        while (getline(iss, curr))
        {
            if (boost::regex_match(curr, match, setPattern))
                insertStatement<register_type, register_content_type, Set>(
                        stoi(match[1]), stoi(match[2]), exe);

            else if (boost::regex_match(curr, match, loadPattern))
                insertStatement<register_type, memory_type, Load>(
                        stoi(match[1]), stoi(match[2]), exe);

            else if (boost::regex_match(curr, match, storePattern))
                insertStatement<memory_type, register_type, Store>(
                        stoi(match[1]), stoi(match[2]), exe);

            else if (boost::regex_match(curr, match, addPattern))
                insertStatement<register_type, register_type, Add>(
                        stoi(match[1]), stoi(match[2]), exe);

            else if (boost::regex_match(curr, match, subPattern))
                insertStatement<register_type, register_type, Sub>(
                        stoi(match[1]), stoi(match[2]), exe);

            else if (boost::regex_match(curr, match, mulPattern))
                insertStatement<register_type, register_type, Mul>(
                        stoi(match[1]), stoi(match[2]), exe);

            else if (boost::regex_match(curr, match, divPattern))
                insertStatement<register_type, register_type, Div>(
                        stoi(match[1]), stoi(match[2]), exe);

            else if (boost::regex_match(curr, match, printlnPattern))
            {
                register_type arg = stoi(match[1]);
                Println instr(arg);
                auto p = make_shared<Println>(instr);
                exe.addInstruction(p);
            }
            else //nothing was matched
                throw new UnknownInstructionException;
        }
        execs_.push_back(exe);
    }
	return std::make_shared<std::vector<ExecutableProgram>>(execs_);
}
