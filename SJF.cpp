#include "SJF.h"
#include "ExecutableProgram.h"
#include "Statement.h"

#include <algorithm>

SJF::SJF()
{
}


SJF::~SJF()
{
}

void SJF::executePrograms(Execs execs, OS& os) {
	std::sort(execs->begin(), execs->end(),
		[](ExecutableProgram p1, ExecutableProgram p2) //sort by time
			{
				return p1.getSize() < p2.getSize();
			}
		);
	for (auto program : *execs.get())
	{
		while (program.hasNextStatement())
			program.getNextStatement()->execute(os);
	}
}
