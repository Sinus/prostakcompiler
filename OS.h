#pragma once

#include "SchedulingAlgorithm.h"

#include <list>
#include <memory>

class OS
{
public:
	OS(std::shared_ptr<SchedulingAlgorithm> a, Registers r, Memory m)
		: alg(a), memory(m), registers(r) {}
	~OS(){}
	void executePrograms(const std::list<std::string>& programs);
    void setMemory(memory_type arg, memory_cell_type val);
    memory_cell_type getMemory(memory_type arg);
    void setRegisters(register_type arg, register_content_type val);
    register_content_type getRegisters(register_type arg);

protected:
	std::shared_ptr<SchedulingAlgorithm> alg;
	Memory memory;
	Registers registers;
};
