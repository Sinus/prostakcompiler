#include <iostream>
#include <list>
#include <memory>

#include "Computer.h"

int main() {
	/*Computer c;
	c.setCPU(4);
	c.setRAM(16);
	auto fcfs = Computer::createFCFSScheduling();
	auto os = c.installOS(fcfs);
	const std::list<std::string> testList =
	{
		"ADD R1 R2\n"
		"MUL R2 R3\n"
		,
		"SET R1 1\n"
		"STORE M1 R1\n"
        };
        os->executePrograms(testList);*/

    std::shared_ptr<Computer> computer(new Computer());

    computer->setCPU(2);

    computer->setRAM(100);

    std::shared_ptr<OS> os = computer->installOS(createRRScheduling(10));

    std::string program = "SET R1 123\nPRINTLN R1";

    std::list<std::string> programs;

    programs.push_back(program);

    os->executePrograms(programs);

    return 0;
}
