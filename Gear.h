#pragma once

#include "types.h"
#include "Exceptions.h"

template<typename T, const int OFFSET>
class Gear {
public:
	T& operator[](int i) {
		i -= OFFSET;
		if (i < 0 || vec.size() <= i)
			throw new IllegalArgumentException();
		return vec[i];
	}
	void resize(int i) {
		if (i < 0)
			throw new IllegalArgumentException();
		vec.resize(i);
	}
	void clear() {
		vec.clear();
	}
    /*void set(int arg, T val){
        arg -= OFFSET;
		if (arg < 0 || vec.size() <= arg)
			throw new IllegalArgumentException();
		vec[arg] = val;    
    }*/
protected:
	std::vector<T> vec;
};

typedef Gear<memory_cell_type, 0> Memory;
typedef Gear<register_content_type, 1> Registers;
