#pragma once
#include "Statement.h"
#include <iostream>

class Println : public Statement{
    public:
    Println(register_type rnum) : num{rnum}
    {
    }

    ~Println(){

    }

    void execute(OS& os) const{
        std::cout<<os.getRegisters(num);
    }

    private:
    register_type num;
    
};
