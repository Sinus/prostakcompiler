#include "Computer.h"
#include "Exceptions.h"
#include "FCFS.h"
#include "RRS.h"
#include "SJF.h"

Computer::Computer()
{
}

Computer::~Computer()
{
}

void Computer::setCPU(register_type numOfRegisters){
	numberOfRegisters = numOfRegisters; //thank you, shared pointers
	registers.clear();
	registers.resize(numberOfRegisters);
}

void Computer::setRAM(memory_type size){
	memorySize = size;
	memory.clear();
	memory.resize(memorySize);
}

std::shared_ptr<OS> Computer::installOS
(std::shared_ptr<SchedulingAlgorithm> alg){
	if (numberOfRegisters == INVALID_VALUE)
		throw new NoCPUException();
	if (memorySize == INVALID_VALUE)
		throw new NoRAMException();
	OS os_(alg, registers, memory);
	os = std::make_shared<OS>(os_);
	return os;
}
