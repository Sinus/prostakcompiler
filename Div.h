#pragma once
#include "Statement.h"
#include "Exceptions.h"

class Div : public Statement{
public:
    Div(register_type rnum1, register_type rnum2) : num1{rnum1}, num2{rnum2}
    {
    }

    ~Div(){

    }

    void execute(OS& os) const{
        if (os.getRegisters(num2) != 0)
            os.setRegisters(num1, os.getRegisters(num1) /
                    os.getRegisters(num2));
        else
            throw new DivisionByZeroException();
    }

    private:
    register_type num1, num2;
};
