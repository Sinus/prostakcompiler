#pragma once
#include "Statement.h"

class Add : public Statement{
    public:
    Add(register_type rnum1, register_type rnum2) : num1{rnum1}, num2{rnum2}
    {
    }

    ~Add(){

    }

    void execute(OS& os) const{
        os.setRegisters(num1, os.getRegisters(num1) + os.getRegisters(num2));
    }

    private:
    register_type num1, num2;
};
