#pragma once
#include "SchedulingAlgorithm.h"
#include "Computer.h"
#include "types.h"


class RRS
	: public SchedulingAlgorithm
{
public:
	RRS(time_type q) : quantum(q) {}
	~RRS();
	virtual void executePrograms(Execs, OS&);
protected:
	time_type quantum;
};
