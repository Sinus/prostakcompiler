#pragma once

#include "Gear.h"

#include <list>
#include <memory>
//typedef unsigned int program_size;

class Statement;

class ExecutableProgram
{
public:
	ExecutableProgram();
	~ExecutableProgram();
	unsigned int getSize() const;
	bool hasNextStatement() const;
    std::shared_ptr<Statement> getNextStatement();
    void addInstruction(std::shared_ptr<Statement>);
private:
    std::list<std::shared_ptr<Statement> > instructions;
};

