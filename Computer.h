#pragma once
#include <memory>
#include "OS.h"
#include "SchedulingAlgorithm.h"
#include "Gear.h"
#include "algorithms.h"

typedef int32_t register_type, memory_type, time_type;

const int INVALID_VALUE = ~0;

class Computer
{
public:
	Computer();
	~Computer();

	void setCPU(register_type numOfRegisters);
	void setRAM(memory_type size);
	std::shared_ptr<OS> installOS(std::shared_ptr<SchedulingAlgorithm> alg);
protected:
	register_type numberOfRegisters = INVALID_VALUE;
	memory_type memorySize = INVALID_VALUE;
	Memory memory;
	Registers registers;
	std::shared_ptr<OS> os;
};

