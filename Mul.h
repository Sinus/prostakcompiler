#pragma once
#include "Statement.h"

class Mul : public Statement{
    public:
    Mul(register_type rnum1, register_type rnum2) : num1{rnum1}, num2{rnum2}
    {
    }

    ~Mul(){

    }

    void execute(OS& os) const{
        os.setRegisters(num1, os.getRegisters(num1) * os.getRegisters(num2));
    }

    private:
    register_type num1, num2;
};
