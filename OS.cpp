#include "OS.h"
#include "PROSTAKCompiler.h"

void OS::executePrograms(const std::list<std::string>& programs){
	Execs execs = compilePrograms(programs);
    alg->executePrograms(execs, *this);
}

void OS::setMemory(memory_type arg, memory_cell_type val){
    memory[arg] = val;
}

memory_cell_type OS::getMemory(memory_type arg){
    return memory[arg];
}

void OS::setRegisters(register_type arg, register_content_type val){
    registers[arg] = val;
}

register_content_type OS::getRegisters(register_type arg){
    return registers[arg];
}
