#pragma once
#include "SchedulingAlgorithm.h"

class SJF
	: public SchedulingAlgorithm
{
public:
	SJF();
	~SJF();
	virtual void executePrograms(Execs, OS&);
};

