#pragma once
#include "Statement.h"

class Set : public Statement{
    public:
    Set(register_type rnum, register_content_type rval) : val{rval}, num{rnum}
    {
    }

    ~Set(){

    }

    void execute(OS& os) const{
        os.setRegisters(num, val);
    }

    private:
    register_content_type val;
    register_type num;
};
